* Feature Name: Combat and character progression

* Start Date: 2019-10-15

* RFC Number: 0017

* Tracking Issue: [#15](https://gitlab.com/veloren/rfcs/issues/15)

# Summary

Players fight against NPCs to level up and gain upgrades so they can fight against stronger NPCs in a more complex way.

# Motivation

Players want to have a challenge and get better at it every day. To make this more interesting and engaging, players learn new active moves along the way.

# Guide-level explanation

## Levels

A player has a different level for each kind of gameplay:

* Combat (fight against enemies, complete quests and finish dungeons)

* Exploration (move though the world)

* Construction (craft, cook or create)

* Social (interact with friendly characters)

* Pure magic (learn about the magic of the world)

Each level represents around 1 hour of gameplay focussed on levelling up.

Every kind of gameplay has it's own upgrade tree and has to be leveled up with the correct skill points.

## Combat-level progression

To explain the essential gameplay concepts to the player, combat-level 1-10 are the tutorial phase. The player can make passive upgrades, but he can't specialize yet. While they are in this phase, NPCs and PCs will give the new player quests that each explain a few concepts. Examples of these are:

* Bring a bucket full of water from the river. (Movement, Interaction)

* Slaughter a helpless pig with a weapon they can choose. (Combat, Inventory)

* Craft food / repair a chair (Crafting)

* Talk to NPCs to find out more about the world and choose a faction (Conversation, Religion)

* Fight against a real threat (Combat)

From combat level 10 to 50 the player will learn new active skills that give combat more variety, but there are still many enemies too strong for the player (the player should retreat). They can also ask NPCs for dungeons near them that have a fitting difficulty. At level 50 the player will be able to kill enemies 10x as strong as a level 10 player could and they have access to a wide variety of active skills that make combat more fun and less repetitive.

After this stage, new active skills get rarer and the player is encouraged to look for difficult enemies and dungeons because of quests they get from NPCs or PCs. Strong enemies will have different attacks or stages and need more attention and player skill to avoid. The player will not get more powerful mechanically in this phase, but he will learn how to use the active skills he has in the best way and order.

## Combat

Weapon types and their basic attacks (which can be activated with LMB, RMB or holding a button):

* Sword: Slash (click LMB), Pierce (hold LMB), Parry/Block (RMB)

* Axe/Hammer: Cut/Smash (LMB), Swing (hold LMB), Parry (RMB)

* Daggers: Stab (LMB), Throw (hold RMB)

* Bow: Shoot (hold LMB)

* Spear: Hit (LMB), Throw (hold RMB)

* Staff: Hit (LMB), Magic (RMB)

* Shield: Parry (click RMB) Defend (hold RMB), Ram (hold RMB while running)

Combat is active. This means that the player can't just stand in place while they fight. They are forced to dodge, block, roll, use their environment and use attacks that will move them from point A to point B. Enemies will act accordingly to archieve this. Some will rush at you, others shoot at your position from a distance, wizards might even burn the floor under you. This keeps combat dynamic and more engaging.

After throwing a weapon, the next weapon in the inventory is automatically equipped.

## Character progression

When reaching a new combat level, the player gains one combat skill point. Upgrades in the skill tree cost different amounts of skill points based on how powerful they are.

Passive upgrades give advantages based on triggers not available to the player. For example "When you take fall damage..." or "When an enemy notices you...". Some passive upgrades are always active ("You gain 2HP/s").

Active upgrades on the other hand are triggered by the player. Most active upgrades are specific to one or two weapon types. The effects include: "You rush into the nearest enemy", "You spawn 3 ghosts that look like you and run around, but don't deal any damage" or "Your hammer smashes the ground causing grounded enemies nearby to stagger".

The upgrades will be laid out so that the player gains a new active skill every 5 levels in the early game and every 10 levels later, so the player always has something to look forward to

## Enemies

Enemy attacks:

* Pigs: Roll and run around when damaged

* Cats: Bite causing bleeding, then keep distance

* Horses: Hit once, then run away

* Cows: Ram into target causing knockback

* Wolfs: Keep distance and circle around target

* Archer (Bow): Keeps a clear sightline and shoots

* Fighter (Sword): Try to hit but can parry and dodge

* Barbarian (Axe): Rams into target causing knockback, slow but powerful attacks

* Wizard (Staff): Either shoot slow magic homing projectiles or put a spell on an ally or choose a 3x3 block radius which start burning