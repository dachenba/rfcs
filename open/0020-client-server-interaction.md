- Feature Name: Client Server Interaction
- Start Date: 2021-03-18
- RFC Number: 0020
- Tracking Issue: https://gitlab.com/veloren/rfcs/issues/18

# Summary
[summary]: #summary

This RFC is describing the interaction between client and server how they exchange data in order to keep the latency down in most situation.
This Proposal covers how ECS components need to be synced for client/server communication, and how to interpolate values

# Motivation
[motivation]: #motivation

We need to have a carefull look in giving players with high latency a good user experience while protect the server from malicious user inputs and hacking attempts.


# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

## Latency

Having a low latency for input -> action is important for a snappy game feeling. We want the client to immediately see teir inputs (e.g. keyboard/mouse) to cause actions (running, fighting, jumping).

![Image of a server and 2 clients, we see the frames and ticks for each. and the times the jump command is send from client, processed on the server and seen on client2](../misc/0020/SingleMessage.svg)

The latency for a simple jump command (RED MESSGAGE) to be transfered from Client1 via Server to Client2 consist of the following components:
-  8.0 ms USB-keyboard
- 10.0 ms client1-ping
- 16.6 ms wait for next server tick
- 20.0 ms server tick calculation
- 15.0 ms client2-ping
-  8.3 ms wait for client2 next frame
- 16.0 ms client2 render time
-  5.0 ms display latency monitor

= 100ms\
On the remote client 14 frames are drawn before the message arrives (under ideal conditions).

(see details in attachment)

### Invalid Solutions

we MUST NOT try to improve latency by
1. fully trust clients and the server just stores theirs calculation\
   The Client could cheat this way
2. let clients talk directly to each other to only requiere 1 connection, rather then 2.\
   This is way to complicated and clients might cheat by sending quickly to the server and delayed to the other client.

### Valid Solutions
1. Increase the tickrate of the server (expensive)
2. Interpolate certain Components based on their last update (prefered)

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

## Client Commands
A "Client Command" is everything that the client can send to the server to interact with the world. e.g.:
 - Move/look around
 - Trading
 - Messages/Group management

Messages like "Request Chunk" do not interact, so they are no Client Command.

Each Client Command is ack by the server. With every tick the Client SPAMS all unacked commands to the server via a Stream that might lose data (e.g. UDP).\
Each Client Command has a TickTime attached. this is the time it was created.\
For each Client Command there exist some State (e.g. position and acceleration for move command), the Client will need to keep a copy of this state until the server acked the respective Client Commands.

## Client is always predicting the future
Core of this concept is, that the client is always predicting. The client does never show the "truth". Only the server knows the truth. But when the truth arived at the client, its already 100ms old. So the client will constantly predict the truth 100ms into the future.
With each update from the server, the Client needs to scrap its old prediction. and predict another 100ms into the future.

## Calculation at the server

The server gets a Actions from each client in combination with their local ticktime.
Ideally, the client local ticktime of the message is greater than the servers local ticktime (even at arival as the client is predicting the future locally).
When the client local ticktime is < the server ticktime, it just get reset to `now`. We don't allow the client to modify the past.

## Interpolation at the client

Calculation as the client is a bit more complex, than on the server, as it involes interpolation.\
The players own actions should been shown by the frontend as happened, until he is corrected by the server.
We don't want to wait for a full roundtrip to show the client what happened!

Additionally the player receives packages from the server, together with the ticktime they were produced on the server.
The client will then locally continue interpolating them.
 - First implementation:
   - other Human Player: Just assume velocity stays constant and pos changes
   - other NPC: reset the NPC to the provided values, and interpolate the dt between last state and current. then continue simulation locally

### Getting corrected by the server
A client may receive an Ack for a ClientCommand that contains another result then the client intended, e.g. he was forbidden to open a door, or was forbidden to jump as he got frozen.

The client needs to locally store additional state from the last acked Commands.
The client also needs to locally store all send Commands which are not acked.
So when a new Ack comes in the client, the client can reinterpolated based on the new update and his local list of Client.

![](../misc/0020/ClientInterpolation1.svg)

The Images shows a Client1 pressing W and a Client2 casting a Stun spell at one point.
Both are sending their commands via a `c` msg. The client answers via `a` msg (here i only showed the answers to client1, client2 also get answers from the server).
The number indicates if a server ack's multiple client at once.
As you can see, in the 4th server tick the Server noticed that a Stun was casted and the server is going to send a `!` message, telling Client1 that he is stunned.
As client1 does not know that currently, he assums he is still running.

![](../misc/0020/ClientInterpolation2.svg)

As soon as Client1 gets the `!` msg it interpolates differently and sets the player to stunned. All new generated client commands now also know that the client is stunned. the old commands are not aware of that yet.
The already send packages where client1 was stunned but not aware, are prob going to be denied by the server.
The stunn will be over simultaniously on client and server, so the last package `c` by client1 will prob be accepted on the server.

### FAQ
#### Exhaustive list of Client Commands
TODO

#### How much does the client preditct
Client starts with sending Client Commands to the server at normal speed.
With the first Ack from the server the clients knows when the Command arrived the server and when it was processed.
For now we can try to keep that value at 20ms, in the future we should dynamically change that if we have network drops.

If a Client Command arrives at the server with an timestamp which was already processed by the server, the server will assume it should be processed on the next tick and send a msg
to the client, which then can can then simulate slightly faster 5% so that the buffer get filled correctly.

#### How to handle dropped messages.
Client Commands are very small, instead of just sending one command each tick, we send a vec of all non-acked Client Commands to the server with each tick. A dropped package can be corrected with the next tick in this case.

#### Order of ECS systems
Our top priority should be reduce input lag of players.
So it makes sense to process Client Commands as first action in the tick. After that is done we can run other ecs systems like physics and agent.
Usually i would argue that the best is to have both systems in parallel, as they should not modify state.
However, when we can fully parallize both systems, we might have a disadvantage parallizing both. Both systems would fight for cores and prob only get 50%. More ECS components would be in use over a longer time and the total time does not differ.
Agent is responsible for NPC predictions, normally i would say the client doesnt notice when a NPC has a 1 tick latency (which is prob true, as the client has an even higer latency).
However in this case we might want to run agent system before physics system and make sure both are fully parallized via rayon.
As soon as the new state is know we should send it to clients, before we sleep till the next tick start

# Drawbacks
[drawbacks]: #drawbacks

- Coding efford. Writing the netcode in a way that each client command is bound to a specific time in order to better predict the respective side needs developers to adjust many places in the code
- This approach requieres the client to locally keep a copy of more data: We requiere at list the comple unacked list of Client Commands (very small) but also some states from the last acked time. E.g. Entity possitions in order to replay the interpolation

# Rationale and alternatives
[alternatives]: #alternatives

- Why is this design the best in the space of possible designs?
  We don't know, but it's better than just not improving our current design.
- What other designs have been considered and what is the rationale for not choosing them?
  I thought about a design where the server will revert back and change the history, though changing history is very complicated and has many side effects
- What is the impact of not doing this?
  We need ugly code to "validate" client input. Validation of Positions is always hard. Hackers could send manipulated messages, which just pass this check. By just sending the Commands we have a clearer interface and less attack surface for a hacker.

# Prior art
[prior-art]: #prior-art

This RFC heavily depends on https://www.youtube.com/watch?v=W3aieHjyNvw (00:22:00)
Though Overwatch has much less of data to sync between the client / server

# Unresolved questions
[unresolved]: #unresolved-questions

- We need to find a exhaustive list of possible Client Commands.

# Attachment

## In Depth calculation of Latency

Having a low latency for input -> action is important for a snappy game feeling. We want the client to immediately see teir inputs (e.g. keyboard/mouse) to cause actions (running, fighting, jumping).
There are multiple systems involved, all add latency to the client server communication:
 - Player clicks spacebar
 - the command takes about 8ms till its processed by the USB-keyboard and available to voxygen.
 - the command needs to be transfered to the server, lets assume a good network with 10ms latency.
 - the server needs to wait till the next server tick, as it processes a tick every 33,3ms. it will take 16,6ms on average till the start of the next tick.
 - the server tick calculation also takes time, anything from 0-33,3ms (or hight when the server is lagging). lets assume 20ms
 - the server sends information out to all clients, lets assume a latency of 15ms to the remote side.
 - the other remote is prob already rendering a frame. It prob runs at 60Hz, which means 1 Frame all 16,6ms and on avg 8,3ms till the start of the next frame
 - the frame prob takes another 16 ms to actually render
 - there is another latency to display the image, usually modern moderns have 5 ms

 In total, this means roughly 100ms of latency in an ideal solution.
 Increasing the server to 60TPS will reduce the latency by 18ms (144TPS => 27ms)
 Having a client to run with only 30 FPS will increase the latenvy by 25ms.
 A slower internet connection up to 300ms for connections from another continent can drastically increase the latency as its doubled for a roundtrip.

![Alt text](../misc/0020/client-server-latency-full.svg)
